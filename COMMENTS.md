# Informações sobre a execução do desafio

A resolução deste desafio foi arquitetada para demonstrar a aplicação de técnicas e conceitos de Devops e GitOps utilizando tecnologias OpenSource bem como a flexibilidade de utilização de recursos dessas tecnologias.

Foram feitas a criação de 5 repositósios, cada qual com seu propósito, sendo eles abaixo: 

https://gitlab.com/publicprojects6/argocd - Este repositório está todas as configurações de estrutura do ArgoCD, Projetos, Apps, Features e Repositórios.
https://gitlab.com/publicprojects6/kube-cluster - Este repositório é utilizado para manter as configurações de manifestos das features implementadas no cluster Kubernetes, como por exemplo, nginx ingress controller, cert-manager...etc.
https://gitlab.com/publicprojects6/desafio-devops-deploy - Repositório com a configuração dos manifestos de deploy pelo ArgoCD no Kubernetes
https://gitlab.com/publicprojects6/desafio-devops - Repositório que contêm o código fonte da API, bem como a esteira de build e release da imagem.
https://gitlab.com/publicprojects6/terraform - Repositório com as configurações de IaC

Foram criados 2 ambientes - STG e PRD - para demonstrar a utilização e esteira de aprovações em ambientes corporativos através de Merge Requests.

# Listas de tecnologias utilizadas:

ArgoCD
Cert Manager
Docker
Docker Registry
GCP
GKE ContainerD
Gcloud SDK
Git
Gitlab
Google Cloud DNS
Google Registry
Kustomize
Let's Encrypt
Load Balancer
Nginx Ingress Controller
Prometheus Operator
Terraform
Visual Studio Code

# Fluxo geral da automação:

Começamos implementando a infraestrutura onde será executado as aplicações, features e apis no Google Cloud. Fazemos isto utilizando Terraform.

No repositório do Teraform há instruções gerais de como utilizar o projeto, que pode ser reaproveitado para outras demandas.

É importante seguir as instruções inicias para criar a organização, o projeto, buckets, contas e chaves de acesso que serão utilizadas pelo Terraform na execução do pipeline.

O pipeline foi preparado para subir ambiente de STG, PRD e até mesmo adicionar mais ambientes. Para isto basta replicar algumas váriáveis por ambiente e duplicar alguns jobs no pipeline.

Após seguir as instruções iniciais o pipeline poderá ser executado. Nele atualmente está configurado para criar um vpc, uma subnet, um cluster GKE, reover o nodegroup padrão, criar um novo nodegroup e no final chamar o pipeline de outro projeto, no caso o que implementa o ArgoCd no cluster que foi criado.

Poderiam as configurações inicias serem automatizadas, como a criação de um novo projeto, buckets, contas etc. Mas mesmo assim deveria existir uma organização e projetos semente através da qual novos projetos seriam criados, então de qualquer forma haveria uma configuração inicial manual. Faria sentido se houvessem muitas implementações de projetos no dia a dia.

Criei também um job manual para destruir a infra depois de testado.

O state fica armazenado no Bucket.

Notem que utilizo bastante recursos de ancora e dependencia de jobs e stages no pipeline, verão isto praticamente em todos os pipelines que escrevo.

Bom, após criar a infra, o último job do pipeline demonstra o uso do recurso de de multi projetos no Gitlab CI, onde chamo o pipeline do projeto do ArgoCD para implmentação do mesmo no GKE.

Após o mesmo ser implementado, o próprio ArgoCD toma conta do deploy, ou seja, termina a implementação dele mesmo aplicando as configurações armmazenadas em seu próprio repositório. Segue detalhado o que é implementado;

- Criação dos projetos do argocd, desafio-devops e features. Utilizados para gestão visual no ArgoCD;
- Configuração dos repositórios do argocd, desafio-devops e features. Utilizados para que o Argo identifique alterações e as aplique no ambiente;
- Autenticação SAML com Google Workspace (Suporta outros tipos de autenticação). Usado para gerenciar acessos a ArgoCD dos usuários e grupos;
- Configuração de permissões de acesso, que por sinal é bastante flexível. Usado para gerenciar permissões no ArgoCD dos usuários e grupos;
- Implementação do Nginx Ingress Controller. Usado com IngressClass no GKE;
- Implementação do Cert-Manager. Faz a gestão/renovação dos certificados HTTPS gerados pelo Let's Encrypt;
- Implementação do Cluster-Issuer. Recursos do Kubernetes que representam autoridades de certificação (CAs) capazes de gerar certificados assinados atendendo às solicitações de assinatura de certificado;
- Implementação do Prometheus Operator. Stack de Monitoramento;
- Implementação da api do desafio.

Obs: Ainda neste projeto, seria interessante integrar uma ferramenta como DNS External com o Cloud DNS para criar as entradas no zona de DNS conforme as mesmas fossem criadas com os objetos de ingress para automatizar também esta parte.

Ainda neste desafio há outro pipeline que executa no projeto da API, no qual faz o build da imagem a partir de um Dockerfile na raíz do projeto e depois faz a release comitando a tag da nova imagem no arquivo de configuração que está no repositório de deploy da AP, onde o ArgoCD faz a leitura da alteração e inicia o processo de rollout da mesma no cluster.

Assim em linhas gerais a aplicação é colocada em execução nos ambientes de STG e PRD.

O ambiente agora está preparado para receber novas apis/apps que devem ser implementadas no GKE utilizando um processo de bem simples.

Com mais tempo poderia tambpem padronizar o pipeline da api para ser utilizado em outros projetos, automatizar a criação do Dockerfile, etc.

# Informações para testar API, conhecer o ARGOCD e repositórios criados

# Repositórios:

- https://gitlab.com/publicprojects6/argocd
- https://gitlab.com/publicprojects6/kube-cluster
- https://gitlab.com/publicprojects6/desafio-devops-deploy
- https://gitlab.com/publicprojects6/desafio-devops
- https://gitlab.com/publicprojects6/terraform

OBS: os objetos de certificado podem aparecer no ARGOCD com status "OutOfSync" Ou "Degraded". Isto é um bug tratado na issue https://github.com/argoproj/argo-cd/issues/2239. Estou testandos as correções.

# Validação da aplicação

Inserção de dados:
STG
curl -sv https://api-stg.qubetecnologia.com.br/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"alice@example.com","comment":"first post!","content_id":1}'
curl -sv https://api-stg.qubetecnologia.com.br/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"alice@example.com","comment":"ok, now I am gonna say something more useful","content_id":1}'
curl -sv https://api-stg.qubetecnologia.com.br/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"bob@example.com","comment":"I agree","content_id":1}'

PRD
curl -sv https://api-stg.qubetecnologia.com.br/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"bob@example.com","comment":"I guess this is a good thing","content_id":2}'
curl -sv https://api-stg.qubetecnologia.com.br/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"charlie@example.com","comment":"Indeed, dear Bob, I believe so as well","content_id":2}'
curl -sv https://api-stg.qubetecnologia.com.br/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"eve@example.com","comment":"Nah, you both are wrong","content_id":2}'

Busca dos dados:
STG
curl -sv https://api-stg.qubetecnologia.com.br/api/comment/list/1

PRD
curl -sv https://api-stg.qubetecnologia.com.br/api/comment/list/2

ARGOCD:

STG
https://argocd-stg.qubetecnologia.com.br/
User: admin
Pass: 2fYJv7d7CbWqVCSd

Deixarei apenas o ambiente de STG em execução. Caso necessário subimos o de PRD na apresentação.