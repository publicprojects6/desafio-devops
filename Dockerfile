FROM ubuntu:22.04 AS base

ENV DEBIAN_FRONTEND noninteractive

WORKDIR /root/code

RUN apt-get update \
  && apt-get install apt-utils -y \
  && apt-get install python3-pip -y \
  && apt-get purge --autoremove -y \
  && apt-get autoremove \
  && apt-get autoclean \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY ["./app/.", "/root/code/."]

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8000

ENTRYPOINT /bin/bash -c "gunicorn --bind 0.0.0.0:8000 --log-level debug api:app"